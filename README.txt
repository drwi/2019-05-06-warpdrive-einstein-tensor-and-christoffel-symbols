---> A few lines of code to compute the Einstein tensor components assciated with the Alcubierre warp drive
 in polar coordinates, as well as the Christoffel symbols of 2nd type.

---> look at "non-zero-Einstein-tensor-doubly-covar2.png" for Einstein tensor doubly covariant components expressions

---> look at "isometries2.png" for the expressions of isometries that can be used in this spacetime



